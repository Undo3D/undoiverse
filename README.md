Undoiverse
==========

An immutable data structure for [Undo3D](https://undo3d.com/), which stores __Entities__ in __Frames of Reference.__

Undoiverse is optimised for three of the most common requests in a multi-user 3D universe:
1. _“What I can see?”_
2. _“Tell me when something I can see changes”_
3. _“I want to change this thing near me”_

A virtual world may contain billions of Entities of vastly different sizes, whose properties (position, colour, etc) may change at any time. Clients have limited memory and processing power, perhaps enough for a few thousand Entities at a time. Undoiverse provides a filtered view of the data. In essence, it knows _ __clients only care about distant Entities when they’re very big.__ _

Undoiverse is great for multi-user creative apps, because it keeps a history of everything that happens. This allows timeline traversal, scrubbing back and forth through the development of the dataset, and of course undo and redo. 


#### Usage

@TODO



#### Frame of Reference

Each [Frame of Reference](https://en.wikipedia.org/wiki/Frame_of_reference) (__FoR__) has the following properties:  
- __`id,`__ unique within its container FoR
- __`path`,__ each __`id`__ from the outermost FoR to this one, in an array
- __`size`,__ the FoR’s depth of nesting, from -7 to 8
- __`container`,__ the FoR which contains this one (null at root)
- __`subFors`,__ an array of 0+ sub-FoRs
- __`subEntities`,__ an array of 0+ sub-Entities
- __`gridX`, `gridY`__ and __`gridZ`,__ coordinates in a 16×16×16 grid
- __`offset`,__ an object describing its deviation from the grid
- __`prevX`, `prevY`__ and __`prevZ`,__ grid-steps to previous FoRs, 0 if none
- __`nextX`, `nextY`__ and __`nextZ`,__ grid-steps to next FoRs, 0 if none
- __`firstInGrid`,__ if true, this FoR’s __`id`__ is its initial grid position
- __`isAligned`,__ true if all __`offset`__ properties are zero
- __`alwaysAligned`,__ true if __`isAligned`__ has always been true
- __`isHid`,__ true if hidden
- __`alwaysHid`,__ true if __`isHid`__ has always been true

These are the sixteen __sizes:__

| #  | ID | Name               | Side-length     | Contains                  |
| -- | -- | ------------------ | --------------- | ------------------------- |
| -7 | D  | __Molecular__      | ~ 1 µm          | Proteins                  |
| -6 | F  | __Bacterial__      | ~ 15 µm         | Bacteria                  |
| -5 | G  | __Cellular__       | ~ 250 µm        | Cells                     |
| -4 | H  | __Locket__         | ~ 5 mm          | Grains of sand            |
| -3 | J  | __Pocket__         | ~ 50 mm         | Pebbles, coins            |
| -2 | K  | __Personal__       | exactly 1 meter | Stones, small animals     |
| -1 | L  | __Room__           | ~ 15 meters     | People, furniture         |
|  0 | M  | __Architectural__  | ~ 250 meters    | Buildings, big trees      |
|  1 | N  | __Pedestrian__     | ~ 5 km          | Fields, a big village     |
|  2 | P  | __Urban__          | ~ 50 km         | Small mountains, a city   |
|  3 | Q  | __Regional__       | ~ 1,000 km      | Countries, seas           |
|  4 | R  | __Continental__    | ~ 15,000 km     | Continents, oceans        |
|  5 | S  | __Terrestrial__    | ~ 250,000 km    | A small planet with moons |
|  6 | T  | __Jovian__         | ~ 5 million km  | A big planet with moons   |
|  7 | V  | __Interplanetary__ | ~ 50 million km | Several planets           |
|  8 | W  | __Stellar__        | ~ 1 billion km  | A small star system       |


#### Offset

@TODO


#### Entity

@TODO
